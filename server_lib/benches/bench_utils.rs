use namechain_utils::block::Block;
use namechain_utils::config::CommonConfig;
use primitive_types::U256;
use server_lib::config::{ServerConfig, ServerConfigBuilder};

#[derive(Clone)]
pub struct TestConfig {
    difficulty: U256,
    block_max_len: usize
}

impl CommonConfig for TestConfig {
    #[inline]
    fn get_difficulty(&self) -> U256 {
        self.difficulty
    }

    #[inline]
    fn get_block_max_len(&self) -> usize {
        self.block_max_len
    }
}

impl TestConfig {
    pub fn new(difficulty: U256, block_max_len: usize) -> Self {
        assert!(block_max_len >= 87); //SHA3-256 one_letter_username u64_20cifre_nounce
        TestConfig{ difficulty, block_max_len }
    }
}

impl Default for TestConfig {
    fn default() -> Self {
        let difficulty: U256 = U256::pow(2.into(), (256-17).into()); // Più è basso, più è difficile
        Self::new(difficulty, 100)
    }
}

pub fn get_default_config(filepath: &str, enable_firewall: bool) -> ServerConfig {
    let mut builder = ServerConfigBuilder::new_with_default().unwrap();
    unsafe {
        builder.explicit_set("bc_path", filepath).unwrap();
        builder.explicit_set("enable_firewall", enable_firewall).unwrap();
        builder.explicit_set("verbosity", 0).unwrap();
    }
    builder.build_config().unwrap()
}

pub const DEFAULT_BLOCK_STR: &str = "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805";

pub fn get_default_block() -> Block {
    Block::from_str(&TestConfig::default(), DEFAULT_BLOCK_STR).unwrap()
}