use criterion::{black_box, criterion_group, criterion_main, Criterion, BenchmarkId, BatchSize};
use std::io::{BufReader, Write, BufRead};
use std::fs::File;
use std::path::PathBuf;

use tempdir::TempDir;
use server_lib::blockchain::BlockChain;
use server_lib::generate::run_generate;
use namechain_utils::block::Block;
use crate::bench_utils::{ get_default_config};
use server_lib::verify::{verify, VerifyResult};
use server_lib::start::firewall::Firewall;
use std::time::Duration;
use std::net::{TcpStream, TcpListener};
use namechain_utils::network::NetMessage;
use server_lib::start::handle_client;
use std::sync::{Arc, Mutex};

mod bench_utils;

// generate.rs benches

pub fn generate_sequential(c: &mut Criterion) {
    let tmpdir = TempDir::new("generate_sequential").unwrap();
    let filepath_obj = tmpdir.path().join("blockchain.bc");
    let filepath = filepath_obj.to_str().unwrap();
    let config = get_default_config(filepath, false);

    let mut group = c.benchmark_group("generate_sequential");
    group.sample_size(10); //TODO: Raise the value in the final version
    group.bench_with_input(BenchmarkId::new("generate", "sequential"), &config,
                       |b, cfg| b.iter(|| run_generate(black_box(cfg.clone()))));
}

//TODO: generate_parallel

criterion_group!(generate_group, generate_sequential);

// verify.rs and blockchain.rs benches

fn extract_limited_blockchain(folder_name: &str, number_of_blocks: usize) -> (TempDir, PathBuf) {
    let tmpdir = TempDir::new(folder_name).unwrap();
    let filepath_obj = tmpdir.path().join("blockchain.bc");
    let mut new_bc = File::create(filepath_obj).unwrap();
    let master_bc = File::open("blockchain.bc").unwrap();
    for line_result in BufReader::new(master_bc).lines().take(number_of_blocks) {
        let line = line_result.unwrap_or_else(
            |_| panic!("During an extract_limited_blockchain with {} blocks, the blockchain construction finished the lines or an unexpected error has arisen.", number_of_blocks));
        writeln!(&mut new_bc, "{}", line).unwrap();
    }
    let filepath_obj = tmpdir.path().join("blockchain.bc");
    (tmpdir, filepath_obj)
}

pub fn blockchain_append_to(c: &mut Criterion) {
    let mut group = c.benchmark_group("blockchain_append_to");
    for n in [1_usize, 10, 100, 1000, 10_000].iter() {
        group.bench_with_input(BenchmarkId::from_parameter(n), n, |b, &n| {
            b.iter_batched(|| {
                let (tmpdir, filepath_obj) = extract_limited_blockchain(format!("blockchain_{}", n).as_str(), n);
                let filepath = filepath_obj.to_str().unwrap();
                let config = get_default_config(filepath, false);
                (tmpdir, BlockChain::new_uncached(&config).unwrap(), Block::default())
            }, |(tmpdir, mut bc, block)| {
                bc.append_block(&black_box(block)).unwrap();
                (black_box(bc), black_box(tmpdir))
            }, BatchSize::PerIteration);
        } );
    }
}

pub fn verify_blockchain(c: &mut Criterion) {
    let mut group = c.benchmark_group("verify_blockchain");
    for n in [1_usize, 10, 100, 1000].iter() {
        group.bench_with_input(BenchmarkId::from_parameter(n), n, |b, &n| {
            b.iter_batched(|| {
                let (tmpdir, filepath_obj) = extract_limited_blockchain(format!("blockchain_{}", n).as_str(), n);
                let filepath = filepath_obj.to_str().unwrap();
                let config = get_default_config(filepath, false);
                (tmpdir, BlockChain::new_uncached(&config).unwrap(), config)
            }, |(tmpdir, bc, config)| {
                let result = verify(&config, bc).unwrap();
                assert_eq!(result, VerifyResult::Verified);
                (black_box(tmpdir), black_box(result))
            }, BatchSize::PerIteration);
        } );
    }
}

criterion_group!(verify_blockchain_group, blockchain_append_to, verify_blockchain);

// start/firewall.rs benches

//TODO: overhead di una singola connection_to_reject con 100 entries in caso accept e reject

//TODO: overhead di una singola remove_unused_entries con 100 entries di cui 50 da rimuovere

//TODO: start/mod.rs benches

//TODO: handle_client con stream locale per ogni tipo di messaggio valido
pub fn handle_client_block(c: &mut Criterion) {
    let tmpdir = TempDir::new("handle_client_block").unwrap();
    let filepath_obj = tmpdir.path().join("blockchain.bc");
    let filepath = String::from(filepath_obj.to_str().unwrap());
    let config = get_default_config(filepath.as_str(), false);
    let clone_filepath = filepath.clone();
    let _handler = std::thread::spawn(move || {
        let listener = TcpListener::bind("0.0.0.0:6300").unwrap();
        let cfg = get_default_config(clone_filepath.as_str(), false);
        let block = Block::new(&cfg, "0000739ed03f06c8e477df62464134daebfdd3c2f9e84775e650e283a84bed7e", "eisterman", 11529215046068604898).unwrap();
        let nmsg = NetMessage::Block(block);
        for stream in listener.incoming() {
            match stream {
                Ok(mut stream) => {
                    nmsg.send_to(&mut stream).unwrap();
                }
                Err(_) => {
                    return;
                }
            }
        }
    });

    c.bench_function("handle_client_block", move |b| {
        b.iter_batched(|| {
            {
                let mut file = File::create(filepath.clone()).unwrap();
                writeln!(&mut file, "0000000000000000000000000000000000000000000000000000000000000000 coinbase 8120").unwrap();
            }
            let bc = Arc::new(Mutex::new(BlockChain::new_cached(&config).unwrap()));
            let stream = TcpStream::connect("127.0.0.1:6300").unwrap();
            let firewall = Arc::new(Mutex::new(Firewall::new(Duration::from_secs(1), 10, false)));
            (config.clone(), stream, bc, firewall)
        },
       |(cfg, stream, bc, firewall)| {
           handle_client(cfg, stream, bc, firewall)
       },
       BatchSize::PerIteration);
    });
    //handler.join().unwrap();
}

criterion_group!(handle_client_group, handle_client_block);

criterion_main!(generate_group, verify_blockchain_group, handle_client_group);

