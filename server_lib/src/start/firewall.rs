use std::collections::HashMap;
use std::net::IpAddr;
use std::time::{Duration, Instant};
use log::trace;

use crate::config::ServerConfig;

//TODO: Add Whitelist
pub struct Firewall {
    database: HashMap<IpAddr,(Instant, u64)>, // For every IP we give the last bad conn. instant and the conn. number counter
    timeout: Duration,
    rejection_level: u64,
    enabled: bool,
}

impl Firewall {
    #[inline]
    pub fn new(timeout: Duration, rejection_level: u64, enabled: bool) -> Self {
        assert_ne!(timeout, Duration::default()); // Check is timeout isn't zero
        assert_ne!(rejection_level, 0);
        Firewall{ database: HashMap::new(), timeout, rejection_level, enabled}
    }

    #[inline]
    pub fn from_config(config: &ServerConfig) -> Self {
        let timeout = Duration::from_secs(config.get_firewall_timeout_seconds());
        let rejection_level = config.get_firewall_reject_level();
        let enabled = config.get_enable_firewall();
        Self::new(timeout, rejection_level, enabled)
    }

    pub fn connection_to_reject(&mut self, client: IpAddr, to_register: bool) -> bool {
        if !self.enabled {
            return false;
        }
        if self.rejection_level == 1 && to_register {
            true
        } else if let Some((time, counter)) = self.database.get_mut(&client) {
            if to_register {
                if time.elapsed() > self.timeout {
                    *counter = 0;
                }
                *time = Instant::now();
                *counter += 1;
            }
            if *counter > self.rejection_level {
                time.elapsed() < self.timeout
            } else {
                false
            }
        } else {
            if to_register {
                self.database.insert(client, (Instant::now(), 1));
            }
            false
        }
    }

    #[inline]
    pub fn remove_unused_entries(&mut self) {
        let timeout = self.timeout;
        self.database.retain(|ip, (time, _counter)| {
            let to_mantain = time.elapsed() < timeout;
            if !to_mantain {
                trace!("Expired ban on: {}", ip);
            }
            to_mantain
        }); // Remove all pairs where fun return false
    }

    // #[inline]
    // pub fn remove_instance(&mut self, client: IpAddr) -> Option<(Instant,u64)> {
    //     self.hashmap.remove(&client)
    // }
}

#[cfg(test)]
mod tests {
    use crate::tests_utils::*;
    use crate::config::{ServerConfig, ServerConfigBuilder};
    use super::Firewall;
    use std::error::Error;
    use std::io::Write;
    use tempdir::TempDir;
    use std::fs::File;
    use std::net::Ipv4Addr;
    use std::time::Duration;
    use std::thread::sleep;

    fn generate_config(test_name: &str) -> Result<(ServerConfig, TempDir), Box<dyn Error>> {
        let mut builder = ServerConfigBuilder::new_with_default().unwrap();
        let tmpdir = TempDir::new(test_name)?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        {
            let config = get_default_config(filepath, false);
            let test_block = get_default_block(&config);
            let mut file = File::create(filepath)?;
            write!(&mut file, "{}", test_block)?;
        }
        unsafe {
            builder.explicit_set("bc_name", filepath).unwrap();
            builder.explicit_set("enable_firewall", true).unwrap();
        }
        let config = builder.build_config()?;
        Ok((config, tmpdir))
    }

    #[test]
    fn test_rejection() -> Result<(), Box<dyn Error>> {
        let (config, _tmphandler) = generate_config("test_rejection")?;
        let mut firewall = Firewall::from_config(&config);
        let rejection_level = config.get_firewall_reject_level();
        let test_ip = Ipv4Addr::new(10, 11, 254, 127);
        assert!(!firewall.connection_to_reject(test_ip.into(), false));
        for _i in 0..rejection_level {
            assert!(!firewall.connection_to_reject(test_ip.into(), true));
            assert!(!firewall.connection_to_reject(test_ip.into(), false));
        }
        assert!(firewall.connection_to_reject(test_ip.into(), true));
        assert!(firewall.connection_to_reject(test_ip.into(), false));
        Ok(())
    }

    #[test]
    fn test_remove_unused_entries() -> Result<(), Box<dyn Error>> {
        let rejection_level = 3;
        let test_ip = Ipv4Addr::new(10, 11, 254, 127);
        let mut firewall = Firewall::new(Duration::from_millis(100), rejection_level, true);
        for _i in 0..rejection_level+1 {
            firewall.connection_to_reject(test_ip.into(), true);
        }
        firewall.remove_unused_entries();
        assert!(firewall.connection_to_reject(test_ip.into(), false));
        sleep(Duration::from_millis(10));
        firewall.remove_unused_entries();
        assert!(firewall.connection_to_reject(test_ip.into(), false));
        sleep(Duration::from_millis(91));
        firewall.remove_unused_entries();
        assert!(!firewall.connection_to_reject(test_ip.into(), false));
        Ok(())
    }

    #[test]
    fn test_rejection_expiration() -> Result<(), Box<dyn Error>> {
        let rejection_level = 3;
        let test_ip = Ipv4Addr::new(10, 11, 254, 127);
        let mut firewall = Firewall::new(Duration::from_millis(100), rejection_level, true);
        for _i in 0..rejection_level+1 {
            firewall.connection_to_reject(test_ip.into(), true);
        }
        assert!(firewall.connection_to_reject(test_ip.into(), false));
        sleep(Duration::from_millis(10));
        assert!(firewall.connection_to_reject(test_ip.into(), false));
        sleep(Duration::from_millis(91));
        assert!(!firewall.connection_to_reject(test_ip.into(), false));
        sleep(Duration::from_millis(10));
        assert!(!firewall.connection_to_reject(test_ip.into(), true));
        sleep(Duration::from_millis(91));
        assert!(!firewall.connection_to_reject(test_ip.into(), false));
        Ok(())
    }

    #[test]
    fn test_disabled_firewall() {
        let test_ip = Ipv4Addr::new(10, 11, 254, 127);
        let mut firewall = Firewall::new(Duration::from_secs(1), 3, false);
        assert!(!firewall.connection_to_reject(test_ip.into(), true));
        assert!(!firewall.connection_to_reject(test_ip.into(), false));
    }
}