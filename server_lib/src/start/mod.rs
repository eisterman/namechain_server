use std::error::Error;
use std::io::prelude::*;
use std::net::{TcpListener, TcpStream, Shutdown};
use std::sync::{Arc,Mutex};
use std::thread;
use std::time::Duration;

use log::{error, info, debug};

use namechain_utils::config::CommonConfig;
use namechain_utils::errors::NetworkError;
use namechain_utils::network::NetMessage;

pub mod firewall;

use crate::blockchain::BlockChain;
use firewall::Firewall;

use crate::config::ServerConfig;

//TODO: Recovery from poisoned Mutex?
pub fn handle_client(config: ServerConfig, mut stream: TcpStream, blockchain: Arc<Mutex<BlockChain>>, firewall: Arc<Mutex<Firewall>>) -> Result<(),Box<dyn Error>> {
    let peer_addr = stream.peer_addr().unwrap().to_string();
    let peer_ip = stream.peer_addr().unwrap().ip();
    debug!("New client connection from: {}", stream.peer_addr().unwrap());
    loop {
        let received_msg = NetMessage::receive_from(&config, &mut stream)?;
        let string_msg = received_msg.to_string();
        //TODO: Make a voluntary disconnection msg
        match received_msg {
            NetMessage::EmptyMessage => {
                debug!("Residual empty stream received, ending the connection with {}", &peer_addr);
                stream.shutdown(Shutdown::Both).unwrap_or(());
                return Err(Box::new(NetworkError::new(peer_addr.as_str(), "")));
            }
            NetMessage::AskLastBlock => {
                let last_block = blockchain.lock().unwrap().get_last_block().unwrap();
                NetMessage::Block(last_block).send_to(&mut stream)?;
            }
            NetMessage::Block(newblock) => {
                let prevblock = blockchain.lock().unwrap().get_last_block().unwrap();
                let prevblock_hash = prevblock.str_hash();
                // Check if the received block is compatible with the last block
                if newblock.get_prevhash() != prevblock_hash {
                    NetMessage::WrongPrevHash.send_to(&mut stream)?;
                    if firewall.lock().unwrap().connection_to_reject(peer_ip, true) {
                        debug!("Wrong hash of block \"{}\" , the firewall is ending the connection with {}", newblock, &peer_addr);
                        stream.shutdown(Shutdown::Both).unwrap_or(());
                        return Err(Box::new(NetworkError::new(peer_addr.as_str(), string_msg.as_str())));
                    } else {
                        debug!("Wrong hash of block \"{}\" from {}", newblock, &peer_addr);
                        continue;
                    }
                }
                // Check if the hash of the block have the right difficulty
                if !newblock.check_difficulty_hash(&config) {
                    NetMessage::WrongDifficulty(config.get_difficulty()).send_to(&mut stream)?;
                    if firewall.lock().unwrap().connection_to_reject(peer_ip, true) {
                        debug!("Wrong difficulty of block \"{}\" , the firewall is ending the connection with {}", newblock, &peer_addr);
                        stream.shutdown(Shutdown::Both).unwrap_or(());
                        return Err(Box::new(NetworkError::new(peer_addr.as_str(), string_msg.as_str())));
                    } else {
                        debug!("Wrong difficulty of block \"{}\" from {}", newblock, &peer_addr);
                        continue;
                    }
                }
                blockchain.lock().unwrap().append_block(&newblock)?;
                debug!("Block \"{}\" from {} appended to the blockchain", newblock, &peer_addr);
                NetMessage::ValidBlockAppended.send_to(&mut stream)?;
            }
            _ => {
                NetMessage::InvalidBlockOrOpcode.send_to(&mut stream)?;
                if firewall.lock().unwrap().connection_to_reject(peer_ip, true) {
                    debug!("Invalid block or opcode, the firewall is ending the connection with {}", &peer_addr);
                    stream.shutdown(Shutdown::Both).unwrap_or(());
                    return Err(Box::new(NetworkError::new(peer_addr.as_str(), string_msg.as_str())));
                } else {
                    debug!("Invalid block or opcode from {}", &peer_addr);
                    continue;
                }
            }
        }
    }
}

pub fn dump_chain_thread(_config: ServerConfig, dump_port: u16, blockchain: Arc<Mutex<BlockChain>>, firewall: Arc<Mutex<Firewall>>) {
    // Bind the server's socket.
    let dump_addr = format!("0.0.0.0:{}", dump_port);

    let dump_listener = TcpListener::bind(dump_addr).unwrap();
    info!("Server dumping on port {}", dump_port);
    for stream in dump_listener.incoming() {
        match stream {
            Ok(mut stream) => {
                if firewall.lock().unwrap().connection_to_reject(stream.peer_addr().unwrap().ip(), true) {
                    debug!("Rejected dump on: {}", stream.peer_addr().unwrap());
                } else {
                    debug!("New dump on: {}", stream.peer_addr().unwrap());
                    let clone_blockchain = blockchain.clone();
                    thread::spawn(move || {
                        // connection succeeded
                        let blockchain_raw = clone_blockchain.lock().unwrap().read_blockchain_bytes().unwrap();
                        let _size = stream.write(&blockchain_raw).unwrap();
                    });
                }
            }
            Err(e) => {
                error!("Error on dump: {}", e);
                /* connection failed */
            }
        }
    }
}

pub fn run_start(config: ServerConfig) -> Result<(), Box<dyn Error>> {
    let dump_port = config.get_dump_port();
    let listen_port = config.get_listen_port();
    let blockchain = Arc::new(Mutex::new(BlockChain::new_cached(&config)?));
    let firewall = Arc::new(Mutex::new(Firewall::from_config(&config)));
    // Launch firewall cleaning thread
    if config.get_enable_firewall() {
        let firewall_clear_interval = Duration::from_millis(config.get_firewall_clear_time_milliseconds());
        let clone_firewall = firewall.clone();
        thread::spawn(move || {
            loop {
                thread::sleep(firewall_clear_interval);
                clone_firewall.lock().unwrap().remove_unused_entries();
            }
        });
        info!("Firewall enabled.");
    } else {
        info!("Firewall disabled.");
    }
    // Launch dump chain thread
    let clone_config = config.clone();
    let clone_blockchain = blockchain.clone();
    let clone_firewall = firewall.clone();
    thread::spawn(move || dump_chain_thread(clone_config, dump_port, clone_blockchain, clone_firewall));
    // Bind the server's socket.
    let listen_addr = format!("0.0.0.0:{}", listen_port);
    {
        let listener_bc = TcpListener::bind(listen_addr).unwrap();
        // accept connections and process them, spawning a new thread for each one
        info!("Server listening on port {}", listen_port);
        for stream in listener_bc.incoming() {
            match stream {
                Ok(stream) => {
                    let peer_ip = stream.peer_addr().unwrap().ip();
                    if firewall.lock().unwrap().connection_to_reject(peer_ip, false) {
                        debug!("Firewall rejected client connection from: {}", peer_ip);
                        continue; //TODO: FirewallRejectionError?
                    }
                    let clone_config = config.clone();
                    let clone_blockchain = blockchain.clone();
                    let clone_firewall = firewall.clone();
                    thread::spawn(move || {
                        // connection succeeded
                        if handle_client(clone_config, stream, clone_blockchain, clone_firewall).is_err() {
                            debug!("Connection terminated."); //TODO: Gestire i print degli errori con nuovi errori e fuori handle?
                        }
                    });
                }
                Err(e) => {
                    error!("Error: {}", e);
                    /* connection failed */
                }
            }
        }
    } // close the socket server
    Ok(())
}
