use std::fs::{File,OpenOptions};
use std::io::{self, BufRead, BufReader, Read, Seek, SeekFrom, Write};
use std::error::Error;

use namechain_utils::block::Block;

use crate::config::ServerConfig;

pub struct BlockChain {
    file: File,
    last_block: Option<Block>,
}

impl BlockChain {
    pub fn new_cached(config: &ServerConfig) -> Result<BlockChain, Box<dyn Error>> {
        let file = OpenOptions::new().read(true).append(true).create(false).open(config.get_bc_path())?;
        let mut bc = BlockChain{file, last_block: None};
        bc.load_cache(config)?;
        Ok(bc)
    }

    pub fn new_uncached(config: &ServerConfig) -> Result<BlockChain, Box<dyn Error>> {
        let file = OpenOptions::new().read(true).append(true).create(true).open(config.get_bc_path())?;
        Ok(BlockChain{file, last_block: None})
    }

    fn load_cache(&mut self, config: &ServerConfig) -> Result<(), Box<dyn Error>> {
        let bufreader = BufReader::new(self.file.try_clone()?);
        if let Some(last_line) =  bufreader.lines().last() {
            let last_block = Block::from_str(config,&last_line?)?;
            self.last_block = Some(last_block);
            Ok(())
        } else {
            Err(Box::new(io::Error::new(io::ErrorKind::InvalidData,
                                        "Error reading the blockchain file: The file is invalid.")))
        }
    }

    //TODO: C'è un modo di creare un View per Block che lo preservi mutabile? Vedi Arc e Weak
    #[inline]
    pub fn get_last_block(&self) -> Option<Block> {
        self.last_block.clone()
    }

    pub fn append_block(&mut self, block: &Block) -> io::Result<()> {
        writeln!(&mut self.file, "{}", block)?;
        self.last_block = Some(block.clone());
        Ok(())
    }

    pub fn read_blockchain_bytes(&self) -> io::Result<Vec<u8>> {
        let mut file = &self.file;
        file.seek(SeekFrom::Start(0))?;
        let mut buffer: Vec<u8> = Vec::new();
        let _size = file.read_to_end(&mut buffer);
        Ok(buffer)
    }

    //TODO: Crea un Buffered BlockChain Reader per evitare l'uso di unsafe
    /// # Safety
    /// The BlockChain object must not be used until the reference returned
    /// by this function are dropped
    pub unsafe fn expose_file(&self) -> &File {
        &self.file
    }
}

//TODO: Move verify utils?

#[cfg(test)]
mod tests {
    use crate::tests_utils::*;
    use super::BlockChain;
    use std::error::Error;
    use std::io::Write;
    use tempdir::TempDir;
    use std::fs::File;

    #[test]
    fn test_new_cached() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_new_cached")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        let test_block = get_default_block(&config);
        {
            let mut file = File::create(filepath_obj)?;
            write!(&mut file, "{}", test_block)?;
        }
        let bc = BlockChain::new_cached(&config)?;
        assert_eq!(bc.get_last_block().unwrap(), test_block);
        Ok(())
    }

    #[test]
    fn test_new_uncached() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_new_uncached")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        let test_block = get_default_block(&config);
        {
            let mut file = File::create(filepath_obj)?;
            write!(&mut file, "{}", test_block)?;
        }
        let bc = BlockChain::new_uncached(&config)?;
        assert!(bc.get_last_block().is_none());
        Ok(())
    }

    #[test]
    fn test_append_block_uncached() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_append_block_uncached")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        let start_block = get_default_block(&config);
        {
            let mut file = File::create(filepath_obj)?;
            write!(&mut file, "{}", start_block)?;
        }
        let new_block = get_default_second_block(&config);
        let mut bc_uc = BlockChain::new_uncached(&config)?;
        assert!(bc_uc.append_block(&new_block).is_ok());
        assert_eq!(bc_uc.get_last_block().unwrap(), new_block);
        Ok(())
    }

    #[test]
    fn test_append_block_cached() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_append_block_cached")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        let start_block = get_default_block(&config);
        {
            let mut file = File::create(filepath_obj)?;
            write!(&mut file, "{}", start_block)?;
        }
        let new_block = get_default_second_block(&config);
        let mut bc_c = BlockChain::new_cached(&config)?;
        assert!(bc_c.append_block(&new_block).is_ok());
        assert_eq!(bc_c.get_last_block().unwrap(), new_block);
        Ok(())
    }

    //TODO: test_read_blockchain_bytes

    // Verify Tests in verify.rs
}