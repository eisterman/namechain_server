pub mod blockchain;
pub mod config;
pub mod generate;
pub mod verify;
pub mod start;

#[cfg(test)]
mod tests_utils {
    use namechain_utils::block::Block;

    use crate::config::{ServerConfig, ServerConfigBuilder};

    pub fn get_default_config(filepath: &str, enable_firewall: bool) -> ServerConfig {
        let mut builder = ServerConfigBuilder::new_with_default().unwrap();
        unsafe {
            builder.explicit_set("bc_path", filepath).unwrap();
            builder.explicit_set("enable_firewall", enable_firewall).unwrap();
        }
        builder.build_config().unwrap()
    }

    pub const DEFAULT_BLOCK_STR: &str = "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805";

    pub fn get_default_block(config: &ServerConfig) -> Block {
        Block::from_str(config, DEFAULT_BLOCK_STR).unwrap()
    }

    pub const DEFAULT_SECOND_BLOCK_STR: &str = "00005ac594667d5587bb7d2baba05cd3f3cdc566150b575e2f3b094d9182ea59 eisterman 16140901064495866136";

    pub fn get_default_second_block(config: &ServerConfig) -> Block {
        Block::from_str(config, DEFAULT_SECOND_BLOCK_STR).unwrap()
    }
}
