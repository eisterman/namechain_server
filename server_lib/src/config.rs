use server_lib_proc_macro::generate_serverconfig_utils;

//TODO: Implement Verbosity in the main code
//TODO: Implement a true log system

generate_serverconfig_utils! {
    difficulty: U256 = "1766847064778384329583297500742918515827483896875618958121606201292619776";
    block_max_len: usize = 100 from 87;
    bc_path: String = "blockchain.bc";
    dump_port: u16 = 6299 from 1 to 65535;
    listen_port: u16 = 6300 from 1 to 65535;
    enable_firewall: bool = true;
    firewall_timeout_seconds: u64 = 600 from 1;
    firewall_clear_time_milliseconds: u64 = 500;
    firewall_reject_level: u64 = 10 from 1;
    verbosity: u8 = 2 from 0 to 5;
}