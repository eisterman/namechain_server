use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use rand::prelude::*;
use sha3::Sha3_256;
use sha3::Digest;

use log::{info};

use namechain_utils::block::Block;

use crate::config::ServerConfig;

pub fn run_generate(config: ServerConfig) -> Result<(), Box<dyn Error>> {
    let bc_name = config.get_bc_path();
    let mut coinbase_vec: Vec<u8> = Vec::with_capacity(32);
    let mut rng = thread_rng();
    coinbase_vec.resize_with(32, | | rng.gen());
    let coinbase_prevhash = format!("{:064x}", 0);
    let mut coinbase_nounce: u64 = 0;
    loop {
        let coinbase_block = Block::new(&config, coinbase_prevhash.as_str(), "coinbase", coinbase_nounce)?;
        let mut hasher = Sha3_256::new();
        hasher.input(format!("{} coinbase {}", coinbase_prevhash, coinbase_nounce));
        if coinbase_block.check_difficulty_hash(&config) {
            break;
        }
        coinbase_nounce += 1;
        //TODO: Permettere all'utente di scegliere tra generazione casuale di hash e nounce
        // oppure dare la possibilità di fornire un prevhash e da li generare il nounce.
        //TODO: Introdurre il multithreading per la generazione del primo blocco (?)
    }
    {
        let mut bc_file = File::create(bc_name.as_str())?;
        writeln!(&mut bc_file, "{} coinbase {}", coinbase_prevhash, coinbase_nounce)?;
    }
    info!("NameChain initial blockchain generated in file {}.", bc_name);
    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::tests_utils::*;
    use super::run_generate;
    use std::error::Error;
    use std::fs::File;
    use std::io::Read;
    use tempdir::TempDir;
    use namechain_utils::block::Block;

    #[test]
    fn test_generate() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_generate")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        run_generate(config.clone()).unwrap();
        let mut bc_file = File::open(filepath_obj)?;
        let mut text = String::new();
        bc_file.read_to_string(&mut text)?;
        let block = Block::from_str(&config, text.as_str())?; //No Trim!
        assert!(block.check_difficulty_hash(&config));
        Ok(())
    }
}