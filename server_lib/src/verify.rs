use std::io::{BufRead, BufReader, Seek, SeekFrom};
use std::error::Error;
use std::fmt::Display;
use std::fmt::Formatter;
use itertools::Itertools;
use primitive_types::U256;
use sha3::Sha3_256;
use sha3::Digest;

use crate::blockchain::BlockChain;
use crate::config::ServerConfig;
use namechain_utils::config::CommonConfig;

#[derive(Debug,PartialEq)]
pub enum VerifyResult {
    Verified,
    InvalidBlock(usize,String),
    FirstPrevhashWrongDifficulty(String),
    WrongPrevhashDifficulty(usize,String),
    WrongHashRelation(usize,String,String), // Hash Expected, Hash Found
}

impl Display for VerifyResult {
    //TODO: MORE ORDER! FOR GOD! MORE ORDER!
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        use VerifyResult::*;
        match self {
            Verified => {
                Ok(())
            },
            InvalidBlock(i, block) => {
                writeln!(f, "Invalid Blockchain! Block number {} invalid: \"{}\"", i, block)
            },
            FirstPrevhashWrongDifficulty(block) => {
                writeln!(f, "Invalid Blockchain! Wrong difficulty of prevhash in 0: \"{}\"", block)
            },
            WrongPrevhashDifficulty(i, block) => {
                writeln!(f, "Invalid Blockchain! Wrong difficulty of prevhash in block {}: \"{}\"", i, block)
            },
            WrongHashRelation(i, block1, block2) => {
                writeln!(f, "Invalid Blockchain! Wrong hash relation from block {} and {}.", i, i+1)?;
                writeln!(f, "Block {} Hash: \"{}\" != Block {} prevhash: \"{}\"", i, block1, i+1, block2)
            },
        }
    }
}

impl VerifyResult {
    pub fn is_verified(&self) -> bool {
        matches!(self, VerifyResult::Verified)
    }
}

pub fn verify(config: &ServerConfig, bc: BlockChain) -> Result<VerifyResult, Box<dyn Error>> {
    use VerifyResult::*;
    let difficulty = config.get_difficulty();
    let mut file = unsafe {
        bc.expose_file()
    };
    file.seek(SeekFrom::Start(0))?; // Is this an edit at a protected object!?
    let bc_file = BufReader::new(file);
    let mut return_state: VerifyResult = Verified; // ZeroValue
    let hash_chain: Vec<Option<(String,String)>> = bc_file.lines().enumerate().map(|(i, line)| {
        if return_state != Verified {
            return None;
        }
        let block = line.unwrap();
        let blk_result = block.split_whitespace().next_tuple();
        if blk_result.is_none() {
            return_state = InvalidBlock(i, block);
            return None;
        }
        let (prevhash, _name, _nounce) = blk_result.unwrap();
        // Difficulty Check
        if i == 0 {
            let prevhash_decimal: U256 = hex::decode(prevhash).unwrap().as_slice().into();
            if prevhash_decimal >= difficulty {
                return_state = FirstPrevhashWrongDifficulty(block);
                return None;
            }
        }
        let prevhash_decimal: U256 = hex::decode(prevhash).unwrap().as_slice().into();
        if prevhash_decimal >= difficulty {
            return_state = WrongPrevhashDifficulty(i, block);
            return None;
        }
        // Construction of the double linked list for checking the consistency of the chain
        let mut hasher = Sha3_256::new();
        hasher.input(block.as_str());
        let block_hash = format!("{:064x}",hasher.result());
        Some((prevhash.to_string(), block_hash))
    }).collect();
    if return_state != Verified {
        return Ok(return_state);
    }
    let hash_chain: Vec<(String,String)> = hash_chain.into_iter().map(|opt| opt.unwrap()).collect();
    // Final and most important consistency check
    for i in 0..(hash_chain.len()-1) {
        if hash_chain[i].1 != hash_chain[i+1].0 {
            return Ok(WrongHashRelation(i, hash_chain[i].1.clone(), hash_chain[i+1].0.clone()));
        }
    }
    Ok(Verified)
}

pub fn run_verify(config: ServerConfig) -> Result<VerifyResult, Box<dyn Error>> {
    let blockchain = BlockChain::new_uncached(&config)?;
    verify(&config, blockchain)
}

#[cfg(test)]
mod tests {
    use crate::tests_utils::*;
    use super::{run_verify, VerifyResult};
    use std::error::Error;
    use std::io::Write;
    use tempdir::TempDir;
    use std::fs::File;

    #[test]
    fn test_verify() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_verify")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        let test_block = get_default_block(&config);
        {
            let mut file = File::create(filepath_obj)?;
            write!(&mut file, "{}", test_block)?;
        }
        assert_eq!(run_verify(config)?, VerifyResult::Verified);
        Ok(())
    }

    #[test]
    fn test_fail_invalid_block_0() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_fail_invalid_block_0")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        {
            let mut file = File::create(filepath_obj)?;
            writeln!(&mut file, "failed_block whitespaced")?;
            write!(&mut file, "00005ac594667d5587bb7d2baba05cd3f3cdc566150b575e2f3b094d9182ea59 eisterman 16140901064495866136")?;
        }
        assert_eq!(run_verify(config)?, VerifyResult::InvalidBlock(0, String::from("failed_block whitespaced")));
        Ok(())
    }

    #[test]
    fn test_fail_invalid_block_1() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_fail_invalid_block_1")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        {
            let mut file = File::create(filepath_obj)?;
            writeln!(&mut file, "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805")?;
            write!(&mut file, "failed_block whitespaced")?;
        }
        eprintln!("{:?}", run_verify(config));
        //assert_eq!(run_verify(config)?, VerifyResult::InvalidBlock(1, String::from("failed_block whitespaced")));
        Ok(())
    }

    #[test]
    fn test_fail_first_prevhash_diff() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_fail_first_prevhash_diff")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        {
            let mut file = File::create(filepath_obj)?;
            writeln!(&mut file, "110029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805")?;
            write!(&mut file, "00005ac594667d5587bb7d2baba05cd3f3cdc566150b575e2f3b094d9182ea59 eisterman 16140901064495866136")?;
        }
        assert!(matches!(run_verify(config)?, VerifyResult::FirstPrevhashWrongDifficulty(_)));
        Ok(())
    }

    #[test]
    fn test_fail_prevhash_diff() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_fail_prevhash_diff")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        {
            let mut file = File::create(filepath_obj)?;
            writeln!(&mut file, "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805")?;
            write!(&mut file, "11005ac594667d5587bb7d2baba05cd3f3cdc566150b575e2f3b094d9182ea59 eisterman 16140901064495866136")?;
        }
        assert!(matches!(run_verify(config)?, VerifyResult::WrongPrevhashDifficulty(_,_)));
        Ok(())
    }

    #[test]
    fn test_fail_wrong_hash_relation() -> Result<(), Box<dyn Error>> {
        let tmpdir = TempDir::new("test_fail_prevhash_diff")?;
        let filepath_obj = tmpdir.path().join("blockchain.bc");
        let filepath = filepath_obj.to_str().unwrap();
        let config = get_default_config(filepath, false);
        {
            let mut file = File::create(filepath_obj)?;
            writeln!(&mut file, "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805")?;
            writeln!(&mut file, "00005ac594667d5587bb7d2baba05cd3f3cdc566150b575e2f3b094d9182ea59 eisterman 16140901064495866136")?;
            writeln!(&mut file, "000064f0f9fa7d79cb9657332eb5520c0625dd431d1b3173fd075c851131e073 eisterman 16140901064496069537")?;
            write!(&mut file, "000025beb4540fefb588ca5e0abad27e9c8e0bbf06ccdcb8f361dc5c585ab7c4 eisterman 16140901064495979438")?;
        }
        // Index, Hash Expected, Hash Found
        assert_eq!(run_verify(config)?, VerifyResult::WrongHashRelation(1,
            String::from("000064f0f9fa7d79cb9657332eb5520c0625dd431d1b3173fd075c851131e072"),
            String::from("000064f0f9fa7d79cb9657332eb5520c0625dd431d1b3173fd075c851131e073")));
        Ok(())
    }
}