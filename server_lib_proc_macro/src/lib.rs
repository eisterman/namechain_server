extern crate proc_macro;
use proc_macro2::TokenStream;
use syn::parse::{Parse, ParseStream, Result};
use syn::punctuated::Punctuated;
use syn::{parse_macro_input, parse_quote, Expr, Lit, Ident, Type};
use syn::Token;
use quote::{quote,format_ident};

mod kw {
    syn::custom_keyword!(from);
    syn::custom_keyword!(to);
}

//noinspection RsUnresolvedReference
/// Parses the following syntax, for the generation of all the ServerConfig utils:
///
///     generate_serverconfig_utils! {
///         $NAME: $TYPE = $EXPR [from $MIN] [to $MAX];
///     }
///
/// For example:
///
///     generate_serverconfig_utils! {
///         difficulty: U256 = 50 from 10 to 70;
///         block_max_len: usize = 100 to 750;
///         dump_port: u16 = 6299 from 1
///         bc_path: String = "blockchain.bc"
///     }
///
/// Note that is mandatory difficulty and block_max_len as first two entries
/// and the type must be U256 and usize.
struct ConfigStructure {
    entries: Punctuated<ConfigEntry,Token![;]>
}

impl Parse for ConfigStructure {
    fn parse(input: ParseStream) -> Result<Self> {
        let entries = input.parse_terminated(ConfigEntry::parse)?;
        Ok(ConfigStructure {
            entries
        })
    }
}

//TODO: Lit or Expr ?
struct ConfigEntry {
    name: Ident,
    ty: Type,
    expr: Lit,
    min: Option<Lit>,
    max: Option<Lit>,
}

impl Parse for ConfigEntry {
    //noinspection RsUnresolvedReference
    fn parse(input: ParseStream) -> Result<Self> {
        let name: Ident = input.parse()?;
        input.parse::<Token![:]>()?;
        let ty: Type = input.parse()?;
        input.parse::<Token![=]>()?;
        let expr: Lit = input.parse()?;
        let lookahead = input.lookahead1();
        let min = if lookahead.peek(kw::from) {
            input.parse::<kw::from>()?;
            Some(input.parse::<Lit>()?)
        } else {
            None
        };
        let lookahead = input.lookahead1();
        let max = if lookahead.peek(kw::to) {
            input.parse::<kw::to>()?;
            Some(input.parse::<Lit>()?)
        } else {
            None
        };
        Ok(ConfigEntry{
            name,
            ty,
            expr,
            min,
            max,
        })
    }
}

// Construction Helper Functions

fn header_token() -> TokenStream {
    quote!{
        use std::error::Error;
        use primitive_types::U256;
        use config::{Config,FileFormat};

        use namechain_utils::config::CommonConfig;
    }
}

fn serverconfig_struct_token(names: &[&Ident], types: &[&Type]) -> TokenStream {
    quote!{
        #[derive(Debug,Clone)]
        pub struct ServerConfig {
            #(#names: #types),*
        }
    }
}

fn serverconfig_impl_token(names_non_mandatory: &[&Ident], types_non_mandatory: &[&Type]) -> TokenStream {
    let function_names: Vec<Ident> = names_non_mandatory.iter().map(|name| format_ident!("get_{}", name)).collect();
    quote!{
        impl ServerConfig {
            #(
            #[inline]
            pub fn #function_names(&self) -> #types_non_mandatory {
                self.#names_non_mandatory.clone()
            }
            )*
        }
    }
}

fn commonconfig_trait_impl_token() -> TokenStream {
    quote!{
        impl CommonConfig for ServerConfig {
            #[inline]
            fn get_difficulty(&self) -> U256 {
                self.difficulty
            }

            #[inline]
            fn get_block_max_len(&self) -> usize {
                self.block_max_len
            }
        }
    }
}

fn serverconfigbuilder_struct() -> TokenStream {
    quote!{
        pub struct ServerConfigBuilder {
            settings: Config
        }
    }
}

//noinspection RsUnresolvedReference
fn serverconfigbuilder_impl_token(entries: &Punctuated<ConfigEntry,Token![;]>, names: &[&Ident]) -> TokenStream {
    let utility_methods = quote!{
        /// # Safety
        /// The value type must be exactly the same of the key-corresponding default value.
        pub unsafe fn get_default_value<'a, T: serde::de::Deserialize<'a>>(key: &str) -> Result<T,Box<dyn Error>>{
            let default = Self::new_with_default()?;
            let result = default.settings.get(key)?;
            Ok(result)
        }

        #[inline]
        pub fn merge_file(&mut self, filename: &str) -> Result<&mut Self,Box<dyn Error>> {
            self.settings.merge(config::File::new(filename,FileFormat::Toml).required(true).exact(true))?;
            Ok(self)
        }

        /// # Safety
        /// The value type inside the other_settings must be exactly the same of the key-corresponding default value.
        #[inline]
        pub unsafe fn merge_settings(&mut self, other_settings: Config) -> Result<&mut Self,Box<dyn Error>> {
            self.settings.merge(other_settings)?;
            Ok(self)
        }

        /// # Safety
        /// The value type must be exactly the same of the key-corresponding default value.
        pub unsafe fn explicit_set<T: Into<config::Value>>(&mut self, key: &str, value: T) -> Result<&mut Self,Box<dyn Error>> {
            self.settings.set(key, value)?;
            Ok(self)
        }

        /// # Safety
        /// The value type must be exactly the same of the key-corresponding default value.
        pub unsafe fn explicit_get<'a, T: serde::de::Deserialize<'a>>(&mut self, key: &str) -> Result<T,Box<dyn Error>> {
            let result = self.settings.get(key)?;
            Ok(result)
        }
    };
    //ServerConfigBuilder new_with_default
    let defaults: Vec<&Lit> = entries.iter().map(|ConfigEntry{expr, ..}| expr).collect();
    let names_string: Vec<String> = names.iter().map(|name| name.to_string()).collect();
    let new_with_default = quote!{
        pub fn new_with_default() -> Result<Self,Box<dyn Error>> {
            let mut settings = Config::new();
            #(settings.set_default(#names_string, #defaults)?;)*
            Ok(Self{settings})
        }
    };
    //ServerConfigBuilder build_config
    #[inline]
    fn getter_type(ty: &Type) -> String {
        #[inline]
        fn typeit(a: Type) -> Type { a };
        String::from(if ty == &typeit(parse_quote!(u8)) || ty == &typeit(parse_quote!(u16)) || ty == &typeit(parse_quote!(u32)) || ty == &typeit(parse_quote!(u64))
            || ty == &typeit(parse_quote!(i8)) || ty == &typeit(parse_quote!(i16)) || ty == &typeit(parse_quote!(i32)) || ty == &typeit(parse_quote!(i64))
            || ty == &typeit(parse_quote!(usize)) || ty == &typeit(parse_quote!(isize)) {
            "int"
        } else if ty == &typeit(parse_quote!(bool)) {
            "bool"
        } else if ty == &typeit(parse_quote!(String)) || ty == &typeit(parse_quote!(U256)){
            "str"
        } else {
            panic!("Used type not ammitted")
        })
    }
    let mut builders: Vec<proc_macro2::TokenStream> = Vec::new();
    for ConfigEntry{ name, ty, min, max, .. } in entries.iter() {
        let get_type = getter_type(ty);
        let stringed_name = name.to_string();
        let get_value = if name == "difficulty" {
            quote!{
                let difficulty =
                    U256::from_dec_str(settings.get_str("difficulty")?.as_str()).expect("Error! \"difficutly\" must a 256bit unsigned integer");
            }
        } else {
            let function_name = format_ident!("get_{}",get_type);
            quote!{
                let #name = settings.#function_name(#stringed_name)?;
            }
        };
        let mut error_string = format!("Error! The \"{}\" must be ", stringed_name);
        let mut condition = String::new();
        if let Some(min_lit) = min {
            let min_lit_string = quote!(#min_lit).into_iter().next().unwrap();
            let tmp_str = format!("greater than {}", min_lit_string);
            error_string.push_str(tmp_str.as_str());
            let tmp_str = format!("{} < {}", stringed_name, min_lit_string);
            condition.push_str(tmp_str.as_str());
        }
        if min.is_some() && max.is_some() {
            error_string.push_str("and ");
            condition.push_str(" || ");
        }
        if let Some(max_lit) = max {
            let max_lit_string = quote!(#max_lit).into_iter().next().unwrap();
            let tmp_str = format!("less than {}", max_lit_string);
            error_string.push_str(tmp_str.as_str());
            let tmp_str = format!("{} > {}", stringed_name, max_lit_string);
            condition.push_str(tmp_str.as_str());
        }
        if min.is_none() && max.is_none() {
            condition = String::from("false");
            error_string = String::from("UNREACHABLE!!!");
        }
        let condition_expr = syn::parse_str::<Expr>(condition.as_str()).unwrap();
        let recaster = if name == "difficulty" {
            quote!{;}
        } else {
            quote!{
                let #name = #name as #ty;
            }
        };
        let single_builder = quote!{
            #get_value
            if #condition_expr {
                panic!(#error_string);
            }
            #recaster
        };
        builders.push(single_builder);
    }
    let build_config = quote!{
        pub fn build_config(&self) -> Result<ServerConfig,Box<dyn Error>> {
            let settings = &self.settings;
            #(#builders)*
            Ok(ServerConfig { #(#names),* })
        }
    };
    quote!{
        impl ServerConfigBuilder {
            #utility_methods
            #new_with_default
            #build_config
        }
    }
}

#[proc_macro]
pub fn generate_serverconfig_utils(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ConfigStructure{ entries} = parse_macro_input!(input as ConfigStructure);
    let names: Vec<&Ident> = entries.iter().map(|ConfigEntry{name, ..}| name).collect();
    let types: Vec<&Type> = entries.iter().map(|ConfigEntry{ty, ..}| ty).collect();
    let names_types_non_mandatory: Vec<(&Ident,&Type)> = entries.iter()
        .map(|ConfigEntry{name, ty, ..}| (name, ty))
        .filter(|(name,_)| {*name != "difficulty" && *name != "block_max_len"} )
        .collect();
    let names_non_mandatory: Vec<&Ident> = names_types_non_mandatory.iter().map(|(name,_)| *name).collect();
    let types_non_mandatory: Vec<&Type> = names_types_non_mandatory.iter().map(|(_,ty)| *ty).collect();

    //Checks
    let contain_difficulty = names.iter().any(|name| *name == "difficulty");
    let contain_block_max_len = names.iter().any(|name| *name == "block_max_len");
    if !(contain_difficulty && contain_block_max_len) {
        panic!("The \"difficulty\" and \"block_max_len\" entries are mandatory.");
    }

    //Header
    let header = header_token();
    //TODO: Check difficulty and block_max_len type
    //ServerConfig struct
    let serverconfig_struct = serverconfig_struct_token(names.as_slice(), types.as_slice());
    //ServerConfig impl
    let serverconfig_impl = serverconfig_impl_token(names_non_mandatory.as_slice(), types_non_mandatory.as_slice());
    //CommonConfig trait impl
    let commonconfig_trait_impl = commonconfig_trait_impl_token();
    //ServerConfigBuilder struct
    let serverconfigbuilder_struct = serverconfigbuilder_struct();
    //ServerConfigBuilder impl
    let serverconfigbuilder_impl = serverconfigbuilder_impl_token(&entries, names.as_slice());

    // Create final code
    let final_code = quote!{
        #header

        #serverconfig_struct
        #serverconfig_impl
        #commonconfig_trait_impl

        #serverconfigbuilder_struct
        #serverconfigbuilder_impl
    };
    final_code.into()
}