use std::error::Error;
use clap::{clap_app, crate_authors, crate_description, crate_version, AppSettings};
use primitive_types::U256;
use config::Config;
use log::{info};

use server_lib::config::ServerConfigBuilder;
use server_lib::generate::run_generate;
use server_lib::verify::run_verify;
use server_lib::start::run_start;

fn main() -> Result<(), Box<dyn Error>> {
    simple_logger::init().unwrap();
    let matches = clap_app!(namechain_server =>
        (setting: AppSettings::ArgRequiredElseHelp)
        (version: crate_version!())
        (author: crate_authors!())
        (about: crate_description!())
        (@arg CONFIG_FILE: -c --config +takes_value "Set the TOML config file (default: config.toml)")
        (@subcommand generate =>
            (about: "generate a new blockchain")
        )
        (@subcommand verify =>
            (about: "verify the global consistency of a blockchain file")
        )
        (@subcommand start =>
            (about: "start the server with the given parameters")
            (@arg DUMP_PORT: -d +takes_value
            "Sets the TCP port used for dumping the blockchain via netcat from default 6299")
            (@arg LISTEN_PORT: -l +takes_value
            "Sets the TCP port used for connecting with the clients from default 6300")
            (@arg DISABLE_FIREWALL: --("disable-firewall")
            "Disable the firewall for DoS/spam attack")
            (@arg FIREWALL_TIMEOUT: -t +takes_value
            "Set the length of time (in seconds) the firewall should ban a malicious client")
            (@arg FIREWALL_REJECT_LEVEL: -r +takes_value
            "Set the number of wrong or heavy connection a client can make before the ban")
        )
    ).get_matches();
    let config_filename =
        matches.value_of("CONFIG_FILE")
            .map_or("config.toml", |arg| arg);
    if config_filename.is_empty() {
        panic!("The config file name cannot be present with an empty name.");
    }
    let mut builder = ServerConfigBuilder::new_with_default()?;
    builder.merge_file(config_filename)?;
    let mut cli_config = Config::new();
    let difficulty: U256 = unsafe {
        let diff_string: String = builder.explicit_get("difficulty")?;
        U256::from_dec_str(diff_string.as_str()).unwrap()
    };
    info!("Difficulty Level: {}", difficulty);
    match matches.subcommand() {
        ("generate", _) => {
            let config = builder.build_config()?;
            run_generate(config)?;
        }
        ("verify", _) => {
            let config = builder.build_config()?;
            let verify_result = run_verify(config)?;
            if verify_result.is_verified() {
                info!("Blockchain file validated correctly.");
            } else {
                info!("Blockchain file invalid.");
                info!("{}", verify_result);
                std::process::exit(1);
            }
        }
        ("start", Some(args)) => {
            if let Some(arg) = args.value_of("DUMP_PORT") {
                cli_config.set("dump_port", arg.parse::<i64>()?)?;
            }
            if let Some(arg) = args.value_of("LISTEN_PORT") {
                cli_config.set("listen_port", arg.parse::<i64>()?)?;
            }
            if args.is_present("DISABLE_FIREWALL") {
                cli_config.set("enable_firewall", false)?;
            }
            if let Some(arg) = args.value_of("FIREWALL_TIMEOUT") {
                cli_config.set("firewall_timeout", arg.parse::<i64>()?)?;
            }
            if let Some(arg) = args.value_of("FIREWALL_REJECT_LEVEL") {
                cli_config.set("firewall_reject_level", arg.parse::<i64>()?)?;
            }
            unsafe { builder.merge_settings(cli_config)?; }
            let config = builder.build_config()?;
            run_start(config)?;
        }
        _ => {
            panic!("The software require at least one subcommand to know what to do!")
        }
    }
    Ok(())
}
